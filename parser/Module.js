(function (factory) {
	'use strict';
	var isAmd = typeof (define) === 'function' && define.amd,
		req = require;
	if (isAmd) {
		define(['./common', './Expression'], factory);
	}
	else if (typeof(exports) === 'object') {
		module.exports = factory.apply(this, ['./common', './Expression'].map(req));
	}
})(function (common, Expression) {
	'use strict';
	var rule = common.rule,
		tryRules = common.tryRules,
		d = common.debug,
		di = common.indent;
	var untilEofRegexp = /^\s*$/;
	var untilEof = {
		tryParse: function (seq) {
			var t = seq.lex(untilEofRegexp);
			if (t) {
				return t;
			}
			else {
				return undefined;
			}
		}
	};
	/*
	 Represents a ninescript file
	 */
	function Module (r) {
		this.expression = r;
	}
	Module.tryParse = function (seq) {
		d('Entering Module at ' + seq.line + ':' + seq.col + ' => ', '', seq.peek(15));
		di(1);
		var r = tryRules([
			rule([Expression, untilEof], common.positive, function (expression) {
				return expression;
			}, ' <Expression> EOF ')
		],seq);
		if (r) {
			d('Success Module', 'success');
		}
		else {
			d('Failed Module', 'error');
		}
		di(-1);
		return r;
	};


	return Module;
});