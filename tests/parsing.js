(function (factory, scope) {
	'use strict';
	var isAmd = typeof (scope.define) === 'function' && scope.define.amd,
		req = require;
	console.log(scope.define);
	if (isAmd) {
		scope.define (['../parser/parser', 'path', 'fs', 'chai'], factory);
	}
	else if (typeof(exports) === 'object') {
		module.exports = factory.apply (this, ['../parser/parser', 'path', 'fs', 'chai'].map (req));
	}
}) (function (parser, path, fs, chai) {
	'use strict';
	var expect = chai.expect;

	function recursiveReaddirSync (dir) {
		var list = [],
			files = fs.readdirSync (dir),
			stats;

		files.forEach (function (file) {
			stats = fs.lstatSync (path.resolve (dir, file));
			if (stats.isDirectory ()) {
				list = list.concat (recursiveReaddirSync (path.resolve (dir, file)));
			} else {
				list.push (path.resolve (dir, file));
			}
		});

		return list;
	}

	function isArray (obj) {
		return (Object.prototype.toString.call (obj) === '[object Array]');
	}

	function ignorePosition (obj) {
		var r = {};
		if (isArray (obj)) {
			return obj.map (function (item) {
				if (typeof(item) === 'object') {
					return ignorePosition (item);
				}
				else {
					return item;
				}
			});
		}
		else {
			Object.keys (obj || {}).forEach (function (k) {
				if ((k !== 'col') && (k !== 'line')) {
					var c = obj[k];
					if (typeof(c) === 'object') {
						r[k] = ignorePosition (c);
					}
					else {
						r[k] = c;
					}
				}
			});
		}
		return r;
	}

	function check (done, f) {
		try {
			f ();
			done ();
		} catch (e) {
			done (e);
		}
	}

	/* global describe, it */
	describe ('ninescript :: syntax', function () {
		describe ('-> Code should match', function () {
			var codeFolder = path.resolve (__dirname, 'code');
			var files = recursiveReaddirSync (codeFolder);
			files.filter (function (f) {
				return path.extname (f) === '.ns';
			}).forEach (function (f) {
				var baseName = path.basename (f, path.extname (f));
				it ('should match syntax test ' + baseName, function (done) {
					var code = fs.readFileSync (f, {encoding: 'utf8'});
					var expected = require (path.resolve (path.dirname (f), baseName + '.json'));
					// returns true
					check (done, function () {
						var actual = parser.parse (code);
						expect (expected).to.deep.equal (ignorePosition (actual));
					});
				});
			});
		});
		describe ('-> Code should fail', function () {
			var failFolder = path.resolve (__dirname, 'fail');
			var files = recursiveReaddirSync (failFolder);
			files.filter (function (f) {
				return path.extname (f) === '.ns';
			}).forEach (function (f) {
				var baseName = path.basename (f, path.extname (f));
				it ('should fail syntax test ' + baseName, function (done) {
					var code = fs.readFileSync (f, {encoding: 'utf8'});

					try {
						parser.parse (code);
						done ('error');
					}
					catch (e) {
						expect (e.message).to.equal ('Parse error');
						done ();
					}
				});
			});
		});
		describe ('-> Code should pass', function () {
			var successFolder = path.resolve (__dirname, 'success');
			var files = recursiveReaddirSync (successFolder);
			files.filter (function (f) {
				return path.extname (f) === '.ns';
			}).forEach (function (f) {
				var baseName = path.basename (f, path.extname (f));
				it ('should pass syntax test ' + baseName, function (done) {
					var code = fs.readFileSync (f, {encoding: 'utf8'});
					check (done, function () {
						var actual = parser.parse (code);
						expect (actual).to.be.an ('object');
					});
				});
			});
		});
	});
}, this);