(function (factory) {
	'use strict';
	var isAmd = typeof (define) === 'function' && define.amd,
		req = require;
	if (isAmd) {
		define(['./common', './Identifier'], factory);
	}
	else if (typeof(exports) === 'object') {
		module.exports = factory.apply(this, ['./common', './Identifier'].map(req));
	}
})(function (common, Identifier) {
	'use strict';
	var rule = common.rule,
		tryRules = common.tryRules,
		consume = common.consume,
		d = common.debug,
		di = common.indent;

	function Expression (expressionType, args) {
		this.type = 'Expression';
		this.expressionType = expressionType;
		switch (expressionType) {
			case 'literal':
				this.content = {
					dataType: args.dataType,
					data: args.data
				};
				break;
			case 'identifier':
				this.content = args.data;
				break;
			case 'binaryOperator':
				this.operator = args.operator;
				this.left = args.left;
				this.right = args.right;
				break;
			case 'destructuring':
				this.pattern = args.pattern;
				this.target = args.target;
				break;
			case 'statementList':
				this.steps = args.steps;
				this.content = args.content;
				break;
			case 'pipeForward':
				this.left = args.left;
				this.right = args.right;
				break;
			case 'composite':
				this.left = args.left;
				this.right = args.right;
				break;
			case 'functionCall':
				this.func = args.func;
				this.parameters = args.parameters;
				break;
		}
	}
	var LiteralString = {
		tryParse: function (seq) {
			var t = seq.lex('\''),
				c,
				r = [];
			if (t) {
				seq = t[1];
				c = seq.peek();
				while (c !== '\'') {
					if (typeof(c) === 'undefined') {
						return undefined;
					}
					if (c === '\\') { //Escaping character
						seq = seq.inc(1);
						c = seq.peek();
					}
					r.push(c);
					seq = seq.inc(1);
					c = seq.peek();
				}
				seq = seq.inc(1);
				return [r.join(''), seq];
			}
		}
	};
	var LiteralNumber = {
		tryParse: function (seq) {
			var t = seq.peek(),
				cnt = 0,
				zeroStart,
				r = [];
			while ((t >= '0') && (t <= '9')) {
				r.push(t);
				if (t === '0') {
					if (cnt === 0) {
						zeroStart = true;
					}
					else if (zeroStart) {
						return undefined;
					}
				}
				seq = seq.inc(1);
				t = seq.peek();
				cnt += 1;
			}
			//Trying for floats
			if (r.length && (t === '.')) {
				r.push(t);
				seq = seq.inc(1);
				cnt = 0;
				t = seq.peek();
				while ((t >= '0') && (t <= '9')) {
					r.push(t);
					seq = seq.inc(1);
					t = seq.peek();
					cnt += 1;
				}
				if (cnt === 0) { //No numbers after the dot
					return undefined;
				}
			}
			if (!r.length) {
				return undefined;
			}
			else {
				return [r.join(''), seq];
			}
		}
	};

	var consumeChar = function () {
		var ops = Array.prototype.slice.call(arguments, 0);
		return {
			tryParse: function (seq) {
				var cnt,
					len = ops.length,
					cur,
					line,
					col;
				seq = consume(seq);
				line = seq.line;
				col = seq.col;
				for (cnt = 0; cnt < len; cnt += 1) {
					cur = ops[cnt];
					if (seq.peek(cur.length) === cur) {
						return [{ value: cur, line: line, col: col }, seq.inc(cur.length)];
					}
				}
			}
		};
	};
	var consumeSkipLines = function () {
		var ops = Array.prototype.slice.call(arguments, 0);
		return {
			tryParse: function (seq) {
				var cnt,
					len = ops.length,
					cur,
					line,
					col;
				seq = consume(seq, { skipLines: true });
				line = seq.line;
				col = seq.col;
				for (cnt = 0; cnt < len; cnt += 1) {
					cur = ops[cnt];
					if (seq.peek(cur.length) === cur) {
						return [{ value: cur, line: line, col: col }, seq.inc(cur.length)];
					}
				}
			}
		};
	};
	var nonConsumingChar = function () {
		var ops = Array.prototype.slice.call(arguments, 0);
		return {
			tryParse: function (seq) {
				var cnt,
					len = ops.length,
					cur,
					line,
					col;
				line = seq.line;
				col = seq.col;
				for (cnt = 0; cnt < len; cnt += 1) {
					cur = ops[cnt];
					if (seq.peek(cur.length) === cur) {
						return [{ value: cur, line: line, col: col }, seq.inc(cur.length)];
					}
				}
			}
		};
	};
	var operatorEquals = consumeChar('=');
	var operatorPipeForward = consumeChar('\\');
	var operatorAdd = consumeChar('+', '-');
	var operatorMult = consumeChar('*', '/');
	var operatorDot = consumeChar('.');
	var operatorStatementList = consumeSkipLines('\n');
	var operatorLeftParens = consumeChar('(');
	var operatorRightParens = consumeChar(')');
	var oneSpaceOrNL = nonConsumingChar(' ', '\n');

	var literalStringMap = function (literal) {
		return new Expression('literal', {
			dataType: {
				type: 'DataType',
				dataType: 'string'
			},
			data: literal
		});
	};
	var literalNumberMap = function (literal) {
		var dataType = 'int';
		if (literal.indexOf('.') >= 0) {
			dataType = 'float';
		}
		return new Expression('literal', {
			dataType: {
				type: 'DataType',
				dataType: dataType
			},
			data: literal
		});
	};
	var identifierMap = function (identifier) {
		return new Expression('identifier', {
			data: identifier
		});
	};
	var parensMap = function (left, exp/*, right*/) {
		/* jshint unused: true */
		return exp;
	};
	var parensAccept = function (left, exp, right) {
		/* jshint unused: true */
		return right.col >= left.col;
	};
	var simpleExpression = {
		tryParse: function (seq) {
			var col = seq.col,
				line = seq.line,
				t;
			t = tryRules([
				rule([operatorLeftParens, Expression, operatorRightParens], parensAccept, parensMap,
					' ( <Expression> ) | Parenthesis '),
				rule([LiteralNumber], common.positive, literalNumberMap,
					' <LiteralNumber> '),
				rule([LiteralString], common.positive, literalStringMap,
					' <LiteralString> '),
				rule([Identifier], common.positive, identifierMap,
					' <Identifier> ')
			],seq);
			if (t) {
				t[0].col = col;
				t[0].line = line;
			}
			return t;
		}
	};
	Expression.tryParse = function (seq) {
		var t,
			exp,
			composite,
			line,
			col,
			pipeForward = function (op, right) {
				return new Expression('pipeForward', {
					operator: op.value,
					left: exp,
					right: right
				});
			},
			pipeForwardAccept = function (op, right) {
				return ((op.col >= col) && (right.col > op.col));
			},
			binaryOperator = function (op, right) {
				return new Expression('binaryOperator', {
					operator: op.value,
					left: exp,
					right: right
				});
			},
			binaryOperatorAccept = function (op, right) {
				return ((op.col > col) && ((right.col > op.col) || ((right.col > col) && (right.line > line))));
			},
			memberAccess = function (op, right) {
				/* jshint unused: true */
				return new Expression('composite', {
					left: exp,
					right: right
				});
			},
			memberAccessAccept = function (op, right) {
				return ((op.col > col) && (right.col > op.col));
			},
			statementList = function (op, right) {
				/* jshint unused: true */
				if (right.expressionType === 'statementList') {
					right.steps.shift(exp);
					return right;
				}
				else {
					return new Expression('statementList', {
						steps: [exp],
						content: right
					});
				}
			},
			statementListAccept = function (op, right) {
				/* jshint unused: true */
				return (right.line > line) && (right.col === col);
			},
			destructuring = function (op, right) {
				/* jshint unused: true */
				return new Expression('destructuring', {
					pattern: exp,
					target: right
				});
			},
			destructuringAccept = function (op, right) {
				/* jshint unused: true */
				return right.col > col;
			},
			functionCall = function (sp, right) {
				/* jshint unused: true */
				if (exp.expressionType === 'functionCall') {
					exp.parameters.push(right);
					return exp;
				}
				else {
					return new Expression('functionCall', {
						func: exp,
						parameters: [right]
					});
				}
			},
			functionCallAccept = function (sp, right) {
				/* jshint unused: true */
				return right.col > col;
			};
		seq = consume(seq);
		line = seq.line;
		col = seq.col;
		d('Entering Expression at ' + seq.line + ':' + seq.col + ' => ', '', seq.peek(15));
		di(1);
		t = simpleExpression.tryParse(seq);
		if (t) {
			exp = t[0];
			seq = t[1];
			//Trying for more complex expressions
			do {
				composite = tryRules([
					rule([oneSpaceOrNL, simpleExpression], functionCallAccept, functionCall, ' \' \' <Expression> | Function call'),
					rule([operatorEquals, Expression], destructuringAccept, destructuring, ' = <Expression> | Destructuring'),
					rule([operatorPipeForward, Expression],  pipeForwardAccept, pipeForward, ' \\ <Expression> | Pipe forward'),
					rule([operatorAdd, Expression], binaryOperatorAccept, binaryOperator, ' +- <Expression> | Add'),
					rule([operatorMult, Expression], binaryOperatorAccept, binaryOperator, ' */ <Expression> | Mult'),
					rule([operatorDot, Expression], memberAccessAccept, memberAccess, ' . <Expression> | Member'),
					rule([operatorStatementList, Expression], statementListAccept, statementList, ' NL <Expression> | StatementList')
				], seq);
				if (composite) {
					exp = composite[0];
					seq = composite[1];
				}
			} while (composite);
			exp.line = line;
			exp.col = col;
			d('Expression success', 'success');
			di(-1);
			return [exp, seq];
		}
		else {
			d('Expression failed', 'error');
			di(-1);
		}
	};
	return Expression;
});