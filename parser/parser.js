(function (factory) {
	'use strict';
	var isAmd = typeof (define) === 'function' && define.amd,
		req = require;
	if (isAmd) {
		define(['./Module'], factory);
	}
	else if (typeof(exports) === 'object') {
		module.exports = factory(req('./Module'));
	}
})(function (Module) {
	'use strict';

	function Str (str) {
		this.val = str;
	}
	function Seq(str, idx) {
		if (str instanceof Str) {
			this.str = str;
			this.line = str.line;
			this.col = str.col;
		}
		else {
			this.str = new Str(str);
			this.line = 0;
			this.col = 0;
		}
		this.idx = idx;
	}
	Seq.prototype.peek = function (len) {
		if (typeof(len) === 'undefined') {
			return this.str.val[this.idx];
		}
		else
		{
			return this.str.val.substr(this.idx, len);
		}
	};
	Seq.prototype.value = function (len) {
		return this.str.val.substr(this.idx, len);
	};
	Seq.prototype.inc = function (len) {
		var cnt,
			line = this.line,
			col = this.col,
			r;
		for (cnt = this.idx; cnt < this.idx + len; cnt += 1) {
			if (this.str.val[cnt] === '\n') {
				line += 1;
				col = 0;
			}
			else {
				col += 1;
			}
		}
		r = new Seq(this.str, this.idx + len);
		r.line = line;
		r.col = col;
		return r;
	};
	/*
	Tests if a specified pattern matches the string, pattern can be a RegExp or a string or a function that returns null if it fails or a string with the matched value. If it's a string is more efficient.
	Returns a tuple (array) of [matched value, next sequence with the characters consumed]
	 */
	Seq.prototype.lex = function (pattern) {
		if (pattern instanceof RegExp) {
			var match = pattern.exec(this.value());
			if (match && (match.index === 0)) {
				return [match[0], this.inc(match[0].length)];
			}
			else {
				return undefined;
			}
		}
		//else if (typeof(pattern) === 'function') {
		//	var t = pattern(this.value());
		//	if (typeof(t) !== undefined) {
		//		return [t, this.inc(t.length)];
		//	}
		//	else {
		//		return undefined;
		//	}
		//}
		else {
			var len = pattern.length,
				cnt,
				iter = this,
				found = true;
			for (cnt = 0; cnt < len; cnt += 1) {
				if (iter.peek() === pattern[cnt]) {
					iter = iter.inc(1);
				}
				else {
					found = false;
					break;
				}
			}
			if (found) {
				return [this.value(len), iter];
			}
			else {
				return undefined;
			}
		}
	};

	function Parser () {

	}
	Parser.prototype.parse = function (str) {
		var seq = new Seq(str, 0),
			r = Module.tryParse(seq);
		if (r) {
			return new Module(r[0]);
		}
		else {
			throw new Error('Parse error');
		}
	};

	return new Parser ();
});