'use strict';
/* global $1, $2, $3 */
/*
	Whatever cb return is assigned to $$ while evaluating rules
 */
function wrap (cb) {
	return '$$ = ('  + cb.toString() + ').apply(this, arguments)';
}
/*
	Returns the very same thing you received
 */
function wrapIdent () {
	return wrap (function () {
		return $1;
	});
}
/*
	Allows a variable to increase indent every time it finds a white space before
 */
function plusIndent () {
	return wrap(function () {
		$2.indent += 1;
		return $2;
	});
}
/*
	WhiteSpace shortcut for variables
 */
function whiteSpace (variableName) {
	return ['_WhiteSpace_ _' + variableName + '_', plusIndent()];
}
/*
	Shortcut for returning rules in lex
 */
function r (str) {
	return 'return "' + str + '\"';
}

var lex = {
	'rules': [
		[' ', 								'_WhiteSpace_'],
		['\\=', 							'_Equals_'],
		['\\\\', 							'_BackSlash_'],
		['\\|>',							'_PipeForward2_'],
		['[\\+\\-\\*\\/\\&\\|\\<\\>]+',		'_Operator_'],
		['\'', 								'_SingleQuote_'],
		['\\n', 							'_NewLineChar_'],
		['$',								'_EOF_'],
		['[a-zA-Z]',						'_Alphabetic_'],
		['[0-9]',							'_Numeric_'],
		['_',								'_Underscore_'],
		['\\$',								'_Dollar_'],
		['\\.',								'_Dot_'],
		['.', 								'_Any_']
	]
};

var grammar = {
	'comment': 'Ninescript language definition',
	'lex': {
		rules: lex.rules.map(function (item) {
			return [item[0], r(item[1])];
		})
	},

	'operators': [],

	'bnf': {
		'expressions': [
			['_Expression_ _IgnorableSpace_', 'return $1']
		],
		'_IgnorableSpace_': [
			['_EOF_', wrap(function () {
				return '';
			})],
			['_NewLine_ _IgnorableSpace_', wrap(function () {
				return '';
			})],
			whiteSpace('IgnorableSpace')
		],
		'_NewLine_': [
			['_NewLineChar_', wrap(function () {
				return '';
			})],
			whiteSpace('NewLine')
		],
		'_Expression_': [
			//whiteSpace('Expression'),
			// literal
			['_LiteralString_', wrap(function () {
				return {
					type: 'Expression',
					expressionType: 'literal',
					content: {
						dataType: {
							type: 'DataType',
							dataType: 'string'
						},
						data: $1
					}
				};
			})],
			// identifier
			['_Identifier_', wrap(function () {
				return {
					type: 'Expression',
					expressionType: 'binaryOperator',
					operator: $2.content,
					left: $1,
					right: $3
				};
			})],
			// binaryOperator
			['_Expression_ _BinaryOperator_ _Expression_', wrap(function () {
				return {
					type: 'Expression',
					expressionType: 'binaryOperator',
					operator: $2.content,
					left: $1,
					right: $3
				};
			})],
			// member
			['_Identifier_ _Dot_ _Identifier_', wrap(function () {
				return {
					type: 'Expression',
					expressionType: 'binaryOperator',
					operator: $2.content,
					left: $1,
					right: $3
				};
			})],
			// member
			['_Expression_ _Dot_ _Identifier_', wrap(function () {
				return {
					type: 'Expression',
					expressionType: 'binaryOperator',
					operator: $2.content,
					left: $1,
					right: $3
				};
			})],
			// pipeForward
			['_Expression_ _WsOrNewLine_ _PipeForward_ _WsOrNewLine_ _Expression_', wrap(function () {
				return {
					type: 'Expression',
					expressionType: 'binaryOperator',
					operator: $2.content,
					left: $1,
					right: $3
				};
			})],
			// expressionList
			['_Assignment_ _NewLine_ _Expression_', wrap(function () {
				return {
					type: 'Expression',
					expressionType: 'binaryOperator',
					operator: $2.content,
					left: $1,
					right: $3
				};
			})],
			// expressionList
			//['_Expression_ _NewLine_ _Expression_', wrap(function () {
			//	return {
			//		type: 'Expression',
			//		expressionType: 'binaryOperator',
			//		operator: $2.content,
			//		left: $1,
			//		right: $3
			//	};
			//})],
		],
		'_NonSingleQuoteChar_': [
			['_BackSlash_ _SingleQuote_', wrap(function () {
				return $2;
			})]
		].concat(
			lex
				.rules
				.filter(function (item) {
					return item[1] !== '_SingleQuote_';
				})
				.map(function (item) {
					return [item[1], wrapIdent()];
				})
		),
		'_NonSingleQuote_': [
			['_NonSingleQuoteChar_', wrap (function (){
				return $1;
			})],
			['_NonSingleQuoteChar_ _NonSingleQuote_', wrap (function () {
				return $1 + $2;
			})]
		],
		'_LiteralString_': [
			['_SingleQuote_ _NonSingleQuote_ _SingleQuote_', wrap (function () {
				return $2;
			})],
			['_SingleQuote_ _SingleQuote_', wrap (function () {
				return '';
			})]
		],
		'_BinaryOperator_': [
			//whiteSpace('BinaryOperator'),
			['_Operator_', wrap(function () {
				return { content: $1 };
			})]
		],
		'_PipeForward_': [
			['_BackSlash_', wrap(function () {
				return { content: $1 };
			})],
			['_PipeForward2_', wrap(function () {
				return { content: $1 };
			})]
		],
		'_Assignment_': [
			//whiteSpace('Assignment'),
			['_Identifier_ _WsOrNewLine_ _AssignOperator_ _WsOrNewLine_ _Expression_', wrap(function () {
				return { content: $1 };
			})]
		],
		'_AssignOperator_': [
			['_Operator_', wrap(function () {
				if ($1 !== '<-') {
					return false;
				}
				else {
					return {};
				}
			})]
		],
		'_WsOrNewLine_': [
			['_WhiteSpace_', wrap(function () {
				return '';
			})],
			['_NewLine_', wrap(function () {
				return '';
			})],
			['_NewLine_ _WsOrNewLine_', wrap(function () {
				return '';
			})],
			whiteSpace('WsOrNewLine')
		],
		'_Identifier_': [
			//whiteSpace('Identifier'),
			['_Alphabetic_', wrap(function () {
				return $1;
			})],
			['_Underscore_', wrap(function () {
				return $1;
			})],
			['_Numeric_', wrap(function () {
				return $1;
			})],
			['_Alphabetic_ _IdentifierRest_', wrap(function () {
				return $1 + $2;
			})],
			['_Underscore_ _IdentifierRest_', wrap(function () {
				return $1 + $2;
			})],
			['_Alphabetic_ _Identifier_', wrap(function () {
				return $1 + $2;
			})],
			['_Underscore_ _Identifier_', wrap(function () {
				return $1 + $2;
			})],
			['_Identifier_ _Numeric_', wrap(function () {
				return $1 + $2;
			})]
		],
		'_IdentifierRest_': [
			['_Alphabetic_ _IdentifierRest_', wrap(function () {
				return $1 + $2;
			})],
			['_Underscore_ _IdentifierRest_', wrap(function () {
				return $1 + $2;
			})],
			['_Numeric_ _IdentifierRest_', wrap(function () {
				return $1 + $2;
			})],
			['_Alphabetic_', wrap(function () {
				return $1;
			})],
			['_Underscore_', wrap(function () {
				return $1;
			})],
			['_Numeric_', wrap(function () {
				return $1;
			})],
		]
	}
};

exports.grammar = grammar;