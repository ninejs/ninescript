/*
 * nineplate grunt configuration file
 */
function exports(grunt) {
	'use strict';

	var jsFiles = ['bin/nscript', '*.js', '**/*.js', '!node_modules/**', '!coverage/**/*.js'],
		testFiles = ['**/tests/**/*.js', '!coverage/**', '!**/tests/**/phantom*.js', '!node_modules/**'],
		Q = require('q');

	// Project configuration.
	grunt.initConfig({
		mochaTest: {
			normal: {
				src: testFiles,
				options: {
					reporter: 'spec',
					globals: []
				}
			}
		},
		istanbul: {
			normal: {
				src: testFiles
			}
		},
		watch: {
			jshint : {
				files : jsFiles,
				tasks : ['jshint']
			},
			test: {
				files: testFiles.concat(jsFiles),
				tasks: ['jshint', 'test', 'cover']
			}

		},
		jshint: {
			files: jsFiles,
			options: {
				bitwise : true,
				camelcase : true,
				forin : true,
				indent : true,
				noempty : true,
				nonew : true,
				plusplus : true,
				maxdepth : 8,
				maxcomplexity : 10,
				strict : true,
				quotmark : 'single',
				regexp : true,
				unused : 'strict',
				curly : true,
				eqeqeq : true,
				immed : true,
				latedef : true,
				newcap : true,
				noarg : true,
				sub : true,
				undef : true,
				boss : true,
				eqnull : true,
				node : true,
				dojo : false,
				passfail : false,
				trailing : true,
				scripturl : true,
				shadow : true,
				browser : false,
				smarttabs : true,
				globals : {
					localStorage : true,
					define : true
				}
			}
		}
	});

	grunt.loadNpmTasks('grunt-mocha-test');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.registerMultiTask('istanbul','copy test coverage instrumentation', function() {
		var path = require('path');
		var done = this.async(),
			files = this.filesSrc,
			queues = [],
			command = path.resolve(__dirname, 'node_modules', 'istanbul', 'lib', 'cli.js'),
			mochaPath = path.resolve(__dirname, 'node_modules', 'mocha', 'bin', '_mocha'),
			r = this.options().require;

		files.forEach(function(file) {
			var childProcess = require('child_process'),
				defer = Q.defer(),
				args = ['cover', mochaPath];
			queues.push(defer.promise);
			if (r) {
				args.push('--require');
				args.push(r);
			}
			args.push(file);
			console.log(args.join(' '));
			var instanbulProcess = childProcess.spawn(command, args, { stdio: 'inherit' });
			instanbulProcess.on('exit', function(/*code*/) {
				defer.resolve();
			});
		});
		Q.all(queues).then(function() {
			done();
		});
	});

	grunt.registerTask('test', ['mochaTest']);
	grunt.registerTask('cover', ['istanbul']);
	// Default task.
	grunt.registerTask('default', ['jshint', 'test']);
}

module.exports = exports;