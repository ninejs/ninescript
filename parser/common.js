(function (factory) {
	'use strict';
	var isAmd = typeof (define) === 'function' && define.amd,
		req = require;
	if (isAmd) {
		define([], factory);
	}
	else if (typeof(exports) === 'object') {
		module.exports = factory(req('colors/safe'));
	}
})(function (colors) {
	'use strict';
	var tokens = {
			startComment: '/*',
			endComment: '*/'
		},
		debugMode = false,
		logIndent = 0,
		identity = function (v) {
			return v;
		};
	if (!colors) {
		colors = {
			green: identity,
			red: identity,
			blue: identity
		};
	}
	function repeat (self, count) {
		/* jshint bitwise: false */
		if (self == null) {
			throw new TypeError('can\'t convert ' + self + ' to object');
		}
		var str = '' + self;
		count = +count;
		if (count !== count) {
			count = 0;
		}
		if (count < 0) {
			throw new RangeError('repeat count must be non-negative');
		}
		if (count === Infinity) {
			throw new RangeError('repeat count must be less than infinity');
		}
		count = Math.floor(count);
		if (str.length === 0 || count === 0) {
			return '';
		}

		var rpt = '';
		for (;;) {
			if ((count & 1) === 1) {
				rpt += str;
			}
			count >>>= 1;
			if (count === 0) {
				break;
			}
			str += str;
		}
		return rpt;
	}
	function consoleDebug (txt, mode, blue) {
		if (debugMode) {
			var t = repeat(' ', logIndent) + txt,
				fn = identity;
			switch (mode) {
				case 'error':
					fn = colors.red;
					break;
				case 'success':
					fn = colors.green;
					break;
			}
			t = fn(t);
			if (blue) {
				t += colors.blue(blue);
			}
			console.log(t);
		}
	}
	function indent (n) {
		logIndent += n;
	}
	function isSpaceChar (t) {
		return (t === ' ') || (t === '\t') || (t === '\r');
	}
	return {
		tokens: tokens,
		rule: function (productions, accept, map, description) {
			return function (sequence) {
				var ok,
					result = [],
					seq = sequence;
				consoleDebug('Trying rule ' + description + ' at ' + seq.line + ':' + seq.col + ' => ', '', seq.peek(15));
				indent(1);
				ok = productions.every(function (p) {
					var t = p.tryParse(seq);
					if (t) {
						seq = t[1];
						result.push(t[0]);
						return typeof(t[0]) !== 'undefined';
					}
				});
				if (!ok) {
					consoleDebug('Rule ' + description + ' failed', 'error');
					indent(-1);
					return undefined;
				}
				else {
					if (!accept || accept.apply(this, result)) {
						consoleDebug('Rule ' + description + ' success', 'success');
						indent(-1);
						return [map.apply(this, result), seq];
					}
					else {
						consoleDebug('Rule ' + description + ' failed by not passing acceptance', 'error');
						indent(-1);
						return undefined;
					}
				}
			};
		},
		tryRules: function (rules, seq) {
			var cnt,
				len = rules.length,
				t;
			for (cnt = 0; cnt < len; cnt += 1) {
				t = rules[cnt](seq);
				if (t) {
					return t;
				}
			}
			return undefined;
		},
		consume: function (seq, options) {
			var index = 0,
				newLines = 0,
				t;
			while (true) {
				t = seq.peek();
				if (isSpaceChar (t)) {
					if (t === '\t') {
						index += 4;
					}
					else {
						index += 1;
					}
					seq = seq.inc(1);
				}
				else if ((t === '\n') && (!options || !options.skipLines)) {
					index = 0;
					newLines += 1;
					seq = seq.inc(1);
				}
				else if (seq.peek(2) === tokens.startComment) {
					index += 2;
					seq = seq.inc(2);
					while (true) {
						t = seq.peek();
						if (typeof(t) === 'undefined') {
							var err = new Error('Parse error');
							err.reason = 'Unfinished comment';
							throw err;
						}
						else if ((t === tokens.endComment[0]) && (seq.peek(tokens.endComment.length) ===  tokens.endComment)) {
							index += tokens.endComment.length;
							seq = seq.inc(tokens.endComment.length);
							break;
						}
						else {
							index += 1;
							seq = seq.inc(1);
						}
					}
				} else {
					break;
				}
			}
			return seq;
		},
		setDebugMode: function (v) {
			debugMode = v;
		},
		debug: consoleDebug,
		indent: indent,
		positive: function () { return true; }
	};
});