**Tokens**

* Expression

	args:
    
    `type: "Expression"`
    
	Expression types are determined by their `expressionType` property


	* Expression types:

		* 	`literal`:
		
			Represents a literal value
			
			Example:
			
				'Hello World'
				
			args:
			
			* `content`:
				* `dataType`: DataType
				
				* `data`: String
		
		*	`identifier`:
		
			Represents an identifier.
			
			Example:
			
				a
			
			args:
			
			* `content`:
				
				* `dataType`: DataType
				* `data`: String. the identifier's name  
		

		*	`composite`:
		
			Represents an object accessing it's members.
			
			Example:
			
				console.log
				
			args:
			
			`left`: Expression. The left part.
			
			`right`: Expression. The right part.
			
			A composite's data type is determined by it's right side's data type.
	
		*	`pipeForward`:
			
			Represents a pipe forward operation. Pipe forwarding can be done with the `\` operator.
			
			Example:
			
				'Hello World'
				\ console.log
				
			args:
			
			`left`: Expression. This is the left part of the pipe forward.
			
			`right`: 'a -> any. This is the right part of the pipe forward. This is a function.
			
			`params`: Expression list
			
		*	`binaryOperator`:
		
			Represents an operator expression, such as math.
			
			Example:
			
				a + 25
				
			args:
			
			`operator`: Operator
			
			`left`: Expression
			
			`right`: Expression
			
			
		* 	`statementList`:
		
			Represents a list of statements that lead to a result.

			Example:
    
			    a = 3
			    b = a * 2
			    a * b
			
			Statements `a = 3` and `b = a * 2` are considered `steps` towards getting this expression's value (which is `a * b`)
			
			args:
			
			`steps`: statement array

			`content`: Expression
    

* DataType

	args:
	
	`type: "DataType"`
	
	`dataType`: Expression or String
	
	
	when dataType is `function`:
	
	`functionParameters`:
	
	* `in`: DataType list
		
	* `out`: DataType list


* Pattern

	args:
		
	`type: "Pattern"`


	Represents pattern matching. The type of pattern matching comes determined by the `patternType` property
	
	* Pattern Types:
		* `equality`
		
			Tests for equality (returning a boolean expression) or assigns left side to right side
			
			Example:
			
				1 + 3 = 4
			or
				
				a = 1 + 3
				
			args:
			
			`left:` Expression (or Destructuring Expression)
			
			`right:` Expression




* Match



		match <Expression> with
		\ <Pattern> -> <Expression>
		\ <Pattern> -> <Expression>
		\ _ -> <Expression>


* Colon
