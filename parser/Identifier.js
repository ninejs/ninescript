(function (factory) {
	'use strict';
	var isAmd = typeof (define) === 'function' && define.amd,
		req = require;
	if (isAmd) {
		define(['./common'], factory);
	}
	else if (typeof(exports) === 'object') {
		module.exports = factory.apply(this, ['./common'].map(req));
	}
})(function (common) {
	'use strict';
	var consume = common.consume;

	function allowedStartCharacter (c) {
		return ((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z')) || (c === '_') || (c === '$');
	}
	function allowedIdentifierCharacter (c) {
		return allowedStartCharacter(c) || ((c >= '0') && (c <= '9'));
	}
	var Identifier = {
		tryParse: function (seq) {
			seq = consume(seq);
			var t = seq.peek(),
				r = [],
				col = seq.col,
				line = seq.line;
			if (allowedStartCharacter(t)) {
				while (allowedIdentifierCharacter(t)) {
					r.push(t);
					seq = seq.inc(1);
					t = seq.peek();
				}
				return [{ value: r.join(''), col: col, line: line}, seq];
			}
		}
	};
	return Identifier;
});